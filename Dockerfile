FROM maven

COPY src /apiTesting/RestAssured/src

COPY pom.xml /apiTesting/RestAssured/pom.xml

COPY testData /apiTesting/RestAssured/testData

WORKDIR /apiTesting/RestAssured/

ENTRYPOINT mvn clean test